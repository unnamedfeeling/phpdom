# Simple PHP DOM parser
This php DOM parser is dead simple. No dependencies.

## Instructions
This program will only work in CLI mode.
This program accepts parameters:
```
--help				- prints out help message
--report			- returns all links on a provided url, expects valid --domain
--parse				- finds all images on the page and outputs a csv in an assets directory, expects valid --url
--url="*some-url*" 	- is used with --parse and --report to provide page's url (if no protocol is provided, it will use https)
```

## Legal information
This program was written by Yarosh Alexander. It is provided as is, without any kind of guarantee.
This program can be used in any non-commercial purposes freely. If you intend to use it in any other way - please contact author for a written permission by email yarosh.alexandr@gmail.com. Commercial usage without author's written permission is prohibited.
