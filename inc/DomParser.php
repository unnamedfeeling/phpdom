<?php
class DomParser {
	public $result;

	private $document;
	private $url;
	private $parsedUrls;
	private $docData;
	private $intermediateResult;

	function __construct($params){
		$this->settings=$params['settings'];
		$this->url=[$this->normalizeUrl($params['url'])];
		$this->result=[];
		$this->parsedUrls=[];
	}

	public function initParse(){
		$this->parse($this->url);
		$this->result=$this->intermediateResult;
		$this->generateCSV(['result'=>$this->result]);
	}

	public function initReport(){
		$this->parse($this->url);
		$this->result=$this->intermediateResult;
	}

	public function help (){
		echo 'This program was written by Yarosh Alexander. It is provided as is, without any kind of guarantee.
This program can be used in any non-commercial purposes freely. If you intend to use it in any other way - please contact author for a written permission by email yarosh.alexandr@gmail.com. Commercial usage without author`s written permission is prohibited.
This program accepts parameters:
 --help					- prints out this message
 --report				- returns all links on a provided url, expects valid --domain
 --parse				- finds all images on the page and outputs a csv in an assets directory, expects valid --domain
 --domain="*some-url*" 	- is used with --parse and --report to provide page`s url (if no protocol is provided, it will use https)';
	}

	public function getResult(){
		return $this->result;
	}

	private function generateCSV($params){
		$now=time();
		$date=date('d-m-Y--H-i-s', $now);
		$fp = fopen($this->settings['outputDir'] . '/report-'.$date.'.csv', 'w');

		fputcsv ( $fp , ['url', 'links', 'images'], ",", '"', "\\" );

		foreach ($params['result'] as $key => $val) {
			$lnks=implode(",",$val['lnks']);
			$imgs=implode(",",$val['imgs']);
			fputcsv ( $fp , [$key, $lnks, $imgs], ",", '"', "\\" );
		}

		fclose($fp);
	}

	private function parse($urls){
		foreach ($urls as $url) {
			if (!in_array($url, $this->parsedUrls)) {
				$url=$this->normalizeUrl($url);
				if (!empty($url)) {
					$this->setupDocData($url);
					$images=$this->filterElementsByUrl([
						'elements'=>$this->parseElements(['doc'=>$this->document, 'tag'=>'img', 'retAttr'=>'src']),
						'url'=>$this->url[0]
					]);
					$links=$this->filterElementsByUrl([
						'elements'=>$this->parseElements(['doc'=>$this->document, 'tag'=>'a', 'retAttr'=>'href']),
						'url'=>$this->url[0]
					]);
					$this->intermediateResult[$url]=[
						'imgs'=>$images,
						'lnks'=>$links
					];
					$this->parsedUrls[]=$url;

					$this->parse($links);
				}
			}
		}
	}

	private function setupDocData($url){
		$this->data=$this->getHtml(['url'=>$url]);

		$this->document=new DOMDocument();
		$this->document->loadHTML($this->data);
	}

	private function normalizeUrl($url){
		$pattern = '/[\w\-]+\.(jpg|png|gif|jpeg)/';
		preg_match($pattern, $url, $matches);
		if (is_array($matches)&&count($matches)>0) {
			return '';
		}
		if(!filter_var($url, FILTER_VALIDATE_URL)&&filter_var($url, FILTER_VALIDATE_DOMAIN)) $url='https://'.$url;
		return $url;
	}

	private function parseElements($params){
		$elements=$this->getElemsByTag(['doc'=>$params['doc'], 'tag'=>$params['tag']]);
		$res=[];

		foreach ($elements as $node) {
			if (!empty($params['retAttr'])) {
				$res[]=$node->getAttribute($params['retAttr']);
			} else {
				$res[]=$node;
			}
		}

		return $res;
	}

	private function getElemsByTag($params){
		return $params['doc']->getElementsByTagName($params['tag']);
	}

	private function filterElementsByUrl($params){
		$elements=$params['elements'];
		$regex='/'.preg_quote($params['url'], '/').'/';
		$result=array_filter($elements, function($v) use ($regex){
			preg_match($regex, $v, $matches);

			return is_array($matches)&&count($matches)>0&&strlen($regex)<strlen($v);
		});

		$result=array_values(array_unique($result));

		return $result;
	}

	private function getHtml($params){
		$data='';
		try {
			$ch = curl_init();

			if ($ch === false) {
				throw new Exception('failed to initialize');
			}

			curl_setopt($ch, CURLOPT_URL, $params['url']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36');

			$data = curl_exec($ch);

			if ($data === false) {
				throw new Exception(curl_error($ch), curl_errno($ch));
			}

			curl_close($ch);
		} catch(Exception $e) {

			// trigger_error(sprintf(
			// 	'Curl failed with error #%d: %s',
			// 	$e->getCode(), $e->getMessage()),
			// 	E_USER_ERROR);
			$data=sprintf('Curl failed with error #%d: %s', $e->getCode(), $e->getMessage());

		}

		return $data;
	}
}
