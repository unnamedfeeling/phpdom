<?php
set_time_limit(0);

if(!defined('STDIN')) {
	die('Run only in CLI mode');
}

$settings=[
	'root'=>__DIR__,
	'outputDir'=>__DIR__.'/assets',
	'options'=>getopt('', ['help', 'parse::', 'report::', 'domain::'])
];

spl_autoload_register(function ($class_name) {
	include $settings['root'] . 'inc/' . $class_name . '.php';
});

$args=['url'=>(!empty($settings['options']['domain']))?$settings['options']['domain']:'', 'settings'=>$settings];

libxml_use_internal_errors(true);

$data=new DomParser($args);

switch (true) {
	case (isset($settings['options']['report'])&&!empty($settings['options']['domain'])):
		$data->initReport();
		print_r($data->getResult());
		break;

	case (isset($settings['options']['parse'])&&!empty($settings['options']['domain'])):
		$data->initParse();
		break;

	default:
		$data->help();
		break;
}

libxml_use_internal_errors(false);
die();
